### General

Screenshot:

![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1614458919/Screen_Shot_2021-02-27_at_10.47.54_PM_uif7ze.png)
![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1614458916/Screen_Shot_2021-02-27_at_10.47.30_PM_yz62so.png)
![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1614458985/Screen_Shot_2021-02-27_at_10.49.24_PM_ajhkxj.png)

### Articles:
https://bezkoder.com/node-express-sequelize-postgresql/ - for database management use Pgadmin


### Configuration Connection String
As an alternative to the --config option with configuration files defining your database, you can use the --url option to pass in a connection string. For example:

$ node_modules/.bin/sequelize db:migrate --url 'mysql://root:password@mysql_host.com/database_name'


### Set up .env

- DATABASE_URL=postgres://postgres:123456@localhost:5432/databasename

- create a model:
- sequelize model:create --name Todo --attributes title:string
  sequelize model:create --name User --attributes firstName:string,lastName:string,email:string
  node_modules/.bin/sequelize model:generate --name User --attributes firstName:string,lastName:string,email:string


* Use React + Next.js to build the different views
* Any code must be in Typescript
* In case you need any API endpoints, create a separated Node.js app
* You must use GraphQL with Apollo Client/Server
* In case you need a database, use Postgres
* Provide very clear and detailed instructions on how to run your solution locally
* No deployment is needed
* IMPORTANT: All solutions must be as production ready as possible. We will mainly take a look at how modular and scalable your app can be, and how you write tests. For the rest, we recommend just to leave a section in the readme with "Future improvements", so that you save some time.
* If you have any implementation detail question, just ask! You can create a GitHub discussion on your repo, add GHTM to it and talk.

## Bonus

Use Styled-components
Provide a Docker compose file to run all apps

  
  
## Part 1
  You need to create a simple acquisition funnel for Marley Spoon!

It will consist of 3 simple steps (take a look to the current Marley Spoon funnel to get an idea; no need to implement the same UI):


## Plan: 


the user should select a plan (for 2 or 4 people), meals per week for the selected plan and show the price per portion and the total price per week.
Enter your data and delivery preferences: the user should enter their email, password, delivery information (first name, last name, address, city and country) and the delivery day from the available ones.
Summary: we show a summary of the previous steps (meals per week, plan type, delivery day, delivery slot and total amount) and a button to continue.
After the summary step, the following should happen:

- A new user is created in the system
- The new user is authenticated in the app (keep it simple and make some notes on how would you improve what you did)
- The user is redirected to the view "recipes", where they can see the list of the available recipes


## Installation

```bash
npm install
```

## Usage

```python
npm run dev
```



### Fake graphql api

https://graphqlzero.almansi.me/

### Fake img API

-https://picsum.photos/id/237/200/300
