module.exports = {
  HOST: "127.0.01",
  USER: "admin",
  PASSWORD: "admin",
  DB: "marleyspoon",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
